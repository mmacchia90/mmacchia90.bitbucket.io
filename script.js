$(document).ready(function () {
    var offset = 120;
    $(window).scroll(function () {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').stop().animate({
                right: '0px'
            }, 100);
        } else {
            $('.back-to-top').stop().animate({
                right: '-58px'
            }, 100);
        }
    });

    $('.back-to-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 300);
        return false;
    })
});


$(document).ready(function () {
    var s = $("#left ul.menu");
    var pos = s.position();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 54) {
            s.css({
                "position": "fixed",
                "top": "6px"
            });
        } else {
            s.css({
                "position": "static",
                "top": pos.top
            });
        }
    });
});